package com.epam.rd.java.basic.task7.db;

import java.io.FileReader;
import java.io.IOException;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.StringJoiner;

import com.epam.rd.java.basic.task7.db.entity.*;

import javax.imageio.IIOException;

import static java.sql.DriverManager.getConnection;


public class DBManager {

	private static DBManager instance;
	private static final String DB_URL;
	private static final Properties properties = new Properties();

	static {
		try{
			properties.load(new FileReader("app.properties"));
		}
		catch (IOException e){
			e.printStackTrace();
		}
		DB_URL = (String) properties.get("connection.url");
	}

	public static synchronized DBManager getInstance() {
		if (instance == null){
			try{
				instance = new DBManager();
			}
			catch(Exception ex){
				ex.printStackTrace();
			}
		}
		return instance;
	}

	private DBManager() {
	}

	public List<User> findAllUsers() throws DBException {
		List<User> users = new ArrayList<>();
		String query = "SELECT u.id, u.login FROM users u ORDER BY u.id";
		try(Connection con = getConnection(DB_URL);
			PreparedStatement pst = con.prepareStatement(query)){
			ResultSet resultSet = pst.executeQuery();
			while (resultSet.next()) {
				User user = new User();
				user.setLogin(resultSet.getString("login"));
				user.setId(resultSet.getInt("id"));
				users.add(user);
			}
		} catch (SQLException ex){
			ex.printStackTrace();
			throw new DBException();
		}
		System.out.println(users);
		return users;
	}

	public boolean insertUser(User user) throws DBException {
		boolean result = false;
		String query = "INSERT INTO users(login) VALUES(?)";
		System.out.println(user);
		try(Connection con = getConnection(DB_URL);
			PreparedStatement pst = con.prepareStatement(query, Statement.RETURN_GENERATED_KEYS)){
			pst.setString(1, user.getLogin());
			int n = pst.executeUpdate();
			ResultSet resultSet = pst.getGeneratedKeys();
			System.out.println(DB_URL);
			while (resultSet.next()) {
				user.setId(resultSet.getInt(1));
			}
			if (n > 0)result = true;
		} catch (SQLException ex){
			ex.printStackTrace();
			throw new DBException();
		}
		return result;
	}

	public boolean deleteUsers(User... users) throws DBException {
		if (users.length == 0) return false;
		StringJoiner stringJoiner = new StringJoiner(",");
		//StringJoiner srtingJoiner = new StringJoiner(",");
		for (User user: users){
			stringJoiner.add(String.valueOf(user.getId()));
		}
		String query = String.format("DELETE FROM users WHERE  id in(%s)", stringJoiner.toString());
		try(Connection con = getConnection(DB_URL);
			PreparedStatement pst = con.prepareStatement(query)){
			pst.executeUpdate();
		} catch (SQLException e){
			e.printStackTrace();
			throw new DBException();
		}
		return true;
	}

	public User getUser(String login) throws DBException {
		User user = new User();
		String query = "SELECT u.id, u.login FROM users u WHERE u.login = ? ORDER BY u.id";
		try(Connection con = getConnection(DB_URL);
			PreparedStatement pst = con.prepareStatement(query)){
			pst.setString(1, login);
			ResultSet resultSet = pst.executeQuery();
			while (resultSet.next()) {
				user = User.createUser(login);
				user.setId(resultSet.getInt(1));
				break;
			}
		} catch (SQLException ex){
			ex.printStackTrace();
			throw new DBException();
		}
		System.out.println(user);
		return user;
	}

	public Team getTeam(String name) throws DBException {
		Team team = new Team();
		String query = "SELECT u.id, u.name FROM teams u WHERE u.name = ? ORDER BY u.id";
		try(Connection con = getConnection(DB_URL);
			PreparedStatement pst = con.prepareStatement(query)){
			pst.setString(1, name);
			ResultSet resultSet = pst.executeQuery();
			while (resultSet.next()) {
				team = Team.createTeam(name);
				team.setId(resultSet.getInt(1));
				break;
			}
		} catch (SQLException ex){
			ex.printStackTrace();
			throw new DBException();
		}
		System.out.println(team);
		return team;
	}

	public List<Team> findAllTeams() throws DBException {
		List<Team> teams = new ArrayList<>();
		String query = "SELECT teams.id, teams.name FROM teams ORDER BY teams.id";
		try(Connection con = getConnection(DB_URL);
			PreparedStatement pst = con.prepareStatement(query)){
			ResultSet resultSet = pst.executeQuery();
			while (resultSet.next()) {
				Team team = new Team();
				team.setName(resultSet.getString("name"));
				team.setId(resultSet.getInt("id"));
				teams.add(team);
			}
		} catch (SQLException ex){
			ex.printStackTrace();
			throw new DBException();
		}
		return teams;
	}

	public boolean insertTeam(Team team) throws DBException {
		boolean result = false;
		String query = "INSERT INTO teams(name) VALUES(?)";
		System.out.println(team);
		try(Connection con = getConnection(DB_URL);
			PreparedStatement pst = con.prepareStatement(query, Statement.RETURN_GENERATED_KEYS)){
			pst.setString(1, team.getName());
			//int n = pst.executeUpdate();
			pst.executeUpdate();
			ResultSet resultSet = pst.getGeneratedKeys();
			//System.out.println(DB_URL);
			while (resultSet.next()) {
				team.setId(resultSet.getInt(1));
			}
			//if (n > 0)result = true;
			result = true;
		} catch (SQLException ex){
			ex.printStackTrace();
			throw new DBException();
		}
		return result;
	}

	public boolean setTeamsForUser(User user, Team... teams) throws DBException {
		if (user == null) return false;
		for (Team team: teams){
			if (team == null) return false;
		}
		boolean result = false;
		Connection con = null;
		PreparedStatement pst = null;
		String query = "INSERT INTO users_teams(user_id, team_id) VALUES(?, ?)";
		try{
			con = getConnection(DB_URL);
			con.setAutoCommit(false);
			pst = con.prepareStatement(query, Statement.RETURN_GENERATED_KEYS);
			for(Team team: teams){
				pst.setInt(1,user.getId());
				pst.setInt(2,team.getId());
				pst.executeUpdate();
			}
			con.commit();
			result = true;
		}
		catch (SQLException ex){
			if (con != null) {
				try {
					con.rollback();
				} catch (SQLException ex2) {
					ex.printStackTrace();
				}
			}

			throw new DBException();
		}
		finally {
			if (pst != null) {
				try {
					pst.close();
				} catch (SQLException ex) {
					ex.printStackTrace();
				}
			}
		}
		//System.out.println(user);
		return result;
	}

	public List<Team> getUserTeams(User user) throws DBException {
		List<Team> teams = new ArrayList<>();
		List<Team> allTeams = findAllTeams();
		String query = "SELECT team_id FROM users_teams WHERE user_id = ?";
		try(Connection con = getConnection(DB_URL);
			PreparedStatement pst = con.prepareStatement(query)){
			pst.setInt(1, user.getId());
			ResultSet resultSet = pst.executeQuery();
			while (resultSet.next()) {
				int teamId = resultSet.getInt(1);
				for (Team team: allTeams){
					if (team.getId() == teamId) {
						teams.add(team);
					}
				}
				//teams.add(getTeamById(resultSet.getInt(1)));
			}
		} catch (SQLException ex){
			ex.printStackTrace();
			throw new DBException();
		}
		//System.out.println(users);
		return teams;
	}

	public boolean deleteTeam(Team team) throws DBException {
		System.out.println("Test 1");
		System.out.println(team);
		String queryTeam = "DELETE FROM teams WHERE  id = " + team.getId();
		String queryTeamsUsers = "DELETE FROM users_teams WHERE  team_id = " + team.getId();
		System.out.println("Test 2");
		try(Connection con = getConnection(DB_URL);
			Statement statement = con.createStatement()){
			statement.addBatch(queryTeam);
			statement.addBatch(queryTeamsUsers);
			statement.executeBatch();
		} catch (SQLException e){
			e.printStackTrace();
			throw new DBException();
		}
		return true;
	}

	public boolean updateTeam(Team team) throws DBException {
		boolean result = false;
		String query = "UPDATE teams t set t.name = ? WHERE t.id = ?";
		try(Connection con = getConnection(DB_URL);
			PreparedStatement pst = con.prepareStatement(query)){
			pst.setString(1, team.getName());
			pst.setInt(2, team.getId());
			pst.executeUpdate();
		} catch (SQLException ex){
			ex.printStackTrace();
			throw new DBException();
		}
		return true;
	}

}
